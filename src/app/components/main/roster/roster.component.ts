import { Component, OnInit } from '@angular/core';
import { RosterManagerService } from 'src/app/core/services/roster-manager.service';

@Component({
  selector: 'app-roster',
  templateUrl: './roster.component.html',
  styleUrls: ['./roster.component.scss']
})
export class RosterComponent implements OnInit {

  constructor(
    public roster: RosterManagerService,
  ) { }

  ngOnInit(): void {
  }

}
