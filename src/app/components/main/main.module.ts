import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RosterComponent } from './roster/roster.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FeatureModule } from '../feature/feature.module';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    RosterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FeatureModule,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    RosterComponent
  ]
})
export class MainModule { }
