import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChecklistComponent } from './checklist/checklist.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CollapseComponent } from './collapse/collapse.component';



@NgModule({
  declarations: [
    ChecklistComponent,
    CollapseComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  exports: [
    ChecklistComponent,
    CollapseComponent,
  ]
})
export class FeatureModule { }
