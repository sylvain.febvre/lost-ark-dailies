import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Action } from 'src/app/core/models/action.model';
import { Checklist } from 'src/app/core/models/checklist.model';
import { ActionManagerService } from 'src/app/core/services/action-manager.service';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.scss']
})
export class ChecklistComponent implements OnInit, OnDestroy {

  @Input() checklist!: Checklist;

  @Input()
  set header(value: boolean | string) {
    this._header = coerceBooleanProperty(value);
  }
  get header(): boolean {
    return this._header;
  }
  private _header: boolean = false;

  @Input() nameWidth?: string;

  editAction?: Action;

  descControls: FormControl[] = [];

  private _destroy$ = new Subject<void>();

  constructor(
    private action: ActionManagerService
  ) { }

  ngOnInit(): void {
    this.descControls = this.checklist.actions.map(action => {
      const fc = new FormControl(action.desc);
      fc.valueChanges.pipe(
        takeUntil(this._destroy$),
        debounceTime(200),
        distinctUntilChanged(),
      ).subscribe({
        next: (value: string) => {
          action.desc = value;
          this.action.save(action);
        }
      })
      return fc;
    });
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  toggleDoneAction(action: Action) {
    if(action.disabled) {
      return;
    }
    action.done = !action.done;
    this.action.save(action);
  }

  toggleDisable(action: Action): void {
    if(!action.canDisable) {
      return;
    }
    action.disabled = !action.disabled;
    this.action.save(action);
  }
}
