import { Component, Input, OnInit } from '@angular/core';
import { LocalStorageCollapse } from 'src/app/core/interfaces/local-storage.interface';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';

@Component({
  selector: 'app-collapse',
  templateUrl: './collapse.component.html',
  styleUrls: ['./collapse.component.scss']
})
export class CollapseComponent implements OnInit {

  @Input() nzHeader?: string;
  @Input() nzBordered: boolean = true;
  @Input() activeKey?: string;
  @Input() done: boolean = false;

  active: boolean = true;

  private static baseKey = 'config-collapse-';

  constructor(
    private _ls: LocalStorageService,
  ) { }

  ngOnInit(): void {
    if (this.activeKey) {
      const activeStorage = this._ls.load<LocalStorageCollapse>(CollapseComponent.baseKey + this.activeKey);
      this.active = activeStorage?.active ?? true;
    }
  }

  onActiveChange(active: boolean): void {
    if (this.activeKey) {
      this._ls.save<LocalStorageCollapse>(CollapseComponent.baseKey + this.activeKey, { active });
    }
  }
}
