import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroModule } from './ng-zorro.module';
import { DoneComponent } from './done/done.component';



@NgModule({
  declarations: [
    DoneComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule
  ],
  exports: [
    NgZorroModule,
    DoneComponent,
  ]
})
export class SharedModule { }
