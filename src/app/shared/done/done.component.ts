import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-done',
  templateUrl: './done.component.html',
  styleUrls: ['./done.component.scss']
})
export class DoneComponent implements OnInit {
  @Input() done: boolean | null = false;
  @Input() text: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
