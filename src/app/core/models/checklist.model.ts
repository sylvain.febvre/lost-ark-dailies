import { Observable, combineLatest, map, distinctUntilChanged, tap, filter } from 'rxjs';
import { IChecklist } from '../interfaces/checklist.interface';
import { Action } from './action.model';

export class Checklist implements IChecklist {
  name?: string;
  desc?: string;
  actions: Action[];

  done$: Observable<boolean>;

  constructor(opts: {
    name?: string;
    desc?: string;
    actions: Action[];
  }) {
    this.name = opts.name;
    this.desc = opts.desc;
    this.actions = opts.actions;

    this.done$ = combineLatest(
      this.actions.map(action => action.doneChange$)
    ).pipe(
      map(doneActions => !doneActions
        .filter(value => !value.disabled)
        .some(value => !value.done)),
      distinctUntilChanged(),
    )
  }
}