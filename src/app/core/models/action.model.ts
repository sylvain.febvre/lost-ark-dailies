import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { IAction } from '../interfaces/action.interface';

export class Action implements IAction {

  readonly name: string;
  readonly icon?: string;
  readonly key: string;
  desc?: string;
  editable: boolean;
  canDisable: boolean;

  private _done = false;
  get done(): boolean { return this._done };
  set done(value: boolean) {
    if (this._done !== value) {
      this._done = value;
      this._doneChanged();
    }
  }

  private _disabled = false;
  get disabled(): boolean { return this._disabled };
  set disabled(value: boolean) {
    if(this._disabled !== value) {
      this._disabled = value;
      this._doneChanged();
    }
  }

  private _doneChange$ = new BehaviorSubject<{
    done: boolean,
    disabled: boolean,
  }>({
    done: this.done,
    disabled: this.disabled,
  })
  doneChange$ = this._doneChange$.asObservable();

  constructor(opts: {
    readonly name: string;
    readonly icon?: string;
    readonly key: string;
    desc?: string;
    editable?: boolean;
    canDisable?: boolean;
  }) {
    this.name = opts.name;
    this.icon = opts.icon;
    this.key = opts.key;
    this.desc = opts.desc;
    this.editable = opts.editable ?? false;
    this.disabled = false;
    this.canDisable = opts.canDisable ?? false;
  }

  private _doneChanged() {
    this._doneChange$.next({
      done: this.done,
      disabled: this.disabled,
    });
  }
}