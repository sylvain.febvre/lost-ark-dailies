import { TestBed } from '@angular/core/testing';

import { RosterManagerService } from './roster-manager.service';

describe('RosterManagerService', () => {
  let service: RosterManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RosterManagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
