import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { LocalStorageAction } from '../interfaces/local-storage.interface';
import { Action } from '../models/action.model';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class ActionManagerService {

  constructor(
    private _ls: LocalStorageService,
  ) { }

  save = (action: Action): Observable<boolean> => {
    const { desc, done, disabled } = action;
    this._ls.save<LocalStorageAction>(action.key, { desc, done, disabled });
    return of(true);
  }

  load = (action: Action): Observable<boolean> => {
    const data = this._ls.load<LocalStorageAction>(action.key);
    if (data) {
      Object.assign(action, data);
    }
    return of(true);
  }
}
