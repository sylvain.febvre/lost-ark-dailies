import { Injectable } from '@angular/core';
import { combineLatest, distinctUntilChanged, map } from 'rxjs';
import { ROSTER } from '../const/roster.ref';
import { ActionManagerService } from './action-manager.service';

@Injectable({
  providedIn: 'root'
})
export class RosterManagerService {

  dailies = ROSTER.dailies;
  weeklies = ROSTER.weeklies;

  // Done observables
  done = {
    rapport$: combineLatest([
      this.dailies.emote.done$,
      this.dailies.song.done$,
    ]).pipe(
      map(doneChecklist => {
        return !doneChecklist.some(done => !done)
      }),
      distinctUntilChanged(),
    ),
    all$: combineLatest([
      ...Object.values(this.dailies).map(cl => cl.done$),
      ...Object.values(this.weeklies).map(cl => cl.done$),
    ]).pipe(
      map(doneChecklist => {
        return !doneChecklist.some(done => !done)
      }),
      distinctUntilChanged(),
    ),
  };

  constructor(
    private _actionManager: ActionManagerService,
  ) {
    Object.values(this.dailies).forEach(checklist => checklist.actions.forEach(this._actionManager.load));
    Object.values(this.weeklies).forEach(checklist => checklist.actions.forEach(this._actionManager.load));
  }


}
