import { Injectable } from '@angular/core';
import { LocalStorage } from '../interfaces/local-storage.interface';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  load<T extends LocalStorage = LocalStorage>(key: string): T | null {
    const item = localStorage.getItem(key);
    if(!item) {
      return null;
    }
    const itemJSON = JSON.parse(item) as T;
    return itemJSON;
  }

  save<T extends LocalStorage = LocalStorage>(key: string, value: Partial<T>): T {
    // Check if it already exists
    const item = localStorage.getItem(key);
    let itemToSave: Partial<T>;
    if(!item) {
      // Creation
      itemToSave = value; 
    } else {
      // Update
      itemToSave = JSON.parse(item) as T;
      Object.assign(itemToSave, value);
    }
    localStorage.setItem(key, JSON.stringify(itemToSave));
    return JSON.parse(localStorage.getItem(key) as string) as T
  }

  delete<T extends LocalStorage = LocalStorage>(key: string): T | null {
    const item = localStorage.getItem(key);
    if(!item) {
      return null;
    }
    const itemJSON = JSON.parse(item) as T;
    localStorage.removeItem(key);
    return itemJSON;
  }
}
