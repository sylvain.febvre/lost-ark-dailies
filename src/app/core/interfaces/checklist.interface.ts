import { Observable } from 'rxjs';
import { Action } from '../models/action.model';

export interface IChecklist {
  name?: string;
  desc?: string;
  actions: Action[];

  done$: Observable<boolean>;
}