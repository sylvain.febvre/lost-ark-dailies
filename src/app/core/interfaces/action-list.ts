import { Checklist } from '../models/checklist.model';

export interface ActionList {
  dailies: Record<string, Checklist>;
  weeklies: Record<string, Checklist>;
}

export interface RosterActionList extends ActionList {
  dailies: {
    other: Checklist,
    song: Checklist,
    emote: Checklist,
    event: Checklist,
  };
  weeklies: {
    main: Checklist,
    vendor: Checklist,
  };
}