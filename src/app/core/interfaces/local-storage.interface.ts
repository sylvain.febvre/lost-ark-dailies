export type LocalStorage = Record<string, string | number | boolean | undefined>;

export interface LocalStorageAction extends LocalStorage {
  done: boolean;
  desc?: string;
  disabled?: boolean;
}

export interface LocalStorageCollapse extends LocalStorage {
  active: boolean;
}