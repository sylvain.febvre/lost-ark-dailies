import { BehaviorSubject, Observable } from 'rxjs';

export interface IAction {
  
  readonly icon?: string;
  readonly key: string;
  readonly name: string;

  desc?: string;
  done: boolean;
  editable: boolean;

  disabled: boolean;
  canDisable: boolean;

  doneChange$: Observable<{
    done: boolean,
    disabled: boolean,
  }>;
}