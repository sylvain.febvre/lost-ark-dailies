import { RosterActionList } from '../interfaces/action-list';
import { Action } from '../models/action.model';
import { Checklist } from '../models/checklist.model';

const SONG = new Checklist({
  name: 'Song',
  desc: 'NPC name',
  actions: [
    new Action({
      name: '1',
      key: 'roster-d-song-1',
      editable: true,
    }),
    new Action({
      name: '2',
      key: 'roster-d-song-2',
      editable: true,
    }),
    new Action({
      name: '3',
      key: 'roster-d-song-3',
      editable: true,
    }),
    new Action({
      name: '4',
      key: 'roster-d-song-4',
      editable: true,
    }),
    new Action({
      name: '5',
      key: 'roster-d-song-5',
      editable: true,
    }),
    new Action({
      name: '6',
      key: 'roster-d-song-6',
      editable: true,
      canDisable: true,
    }),
  ]
});

const EMOTE = new Checklist({
  name: 'Emote',
  desc: 'NPC name',
  actions: [
    new Action({
      name: '1',
      key: 'roster-d-emote-1',
      editable: true,
    }),
    new Action({
      name: '2',
      key: 'roster-d-emote-2',
      editable: true,
    }),
    new Action({
      name: '3',
      key: 'roster-d-emote-3',
      editable: true,
    }),
    new Action({
      name: '4',
      key: 'roster-d-emote-4',
      editable: true,
    }),
    new Action({
      name: '5',
      key: 'roster-d-emote-5',
      editable: true,
    }),
    new Action({
      name: '6',
      key: 'roster-d-emote-6',
      editable: true,
      canDisable: true,
    }),
  ]
});

const OTHER_DAILIES = new Checklist({
  actions: [
    new Action({
      name: 'Chaos gate',
      key: 'roster-d-chaos-gate',
      desc: 'Every MON, THU, SAT, SUN',
    }),
    new Action({
      name: 'Field boss',
      key: 'roster-d-field-boss',
    }),
    new Action({
      name: 'Adventure island',
      key: 'roster-d-island',
    }),
  ]
});

const EVENT_DAILY = new Checklist({
  actions: [
    new Action({
      name: 'Arkesia grand prix',
      key: 'roster-d-arkesia-gp',
    })
  ]
});

const OTHER_WEEKLIES = new Checklist({
  actions: [
    new Action({
      name: 'Ghost ship',
      key: 'roster-w-ghost-ship',
    }),
  ]
});

const VENDOR_WEEKLY = new Checklist({
  actions: [
    new Action({
      name: 'Arkesia event vendor',
      key: 'roster-w-arkesia-event-vendor',
    }),
    new Action({
      name: 'Guardian event vendor',
      key: 'roster-w-guardian-event-vendor',
    }),
    new Action({
      name: 'PvP token vendor',
      key: 'roster-w-pvp-vendor',
    })
  ]
})

export const ROSTER: RosterActionList = {
  dailies: {
    song: SONG,
    emote: EMOTE,
    other: OTHER_DAILIES,
    event: EVENT_DAILY,
  },
  weeklies: {
    main: OTHER_WEEKLIES,
    vendor: VENDOR_WEEKLY,
  }
}