import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewRoutingModule } from './view-routing.module';
import { HomeModule } from './home/home.module';
import { CharacterModule } from './character/character.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ViewRoutingModule,
    // CharacterModule,
    // HomeModule, 
  ],
  exports: [ViewRoutingModule]
})
export class ViewModule { }
